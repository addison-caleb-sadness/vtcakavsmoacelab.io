+++
title = "Operating Systems"
transparent = true
+++

This might go badly.

## Disclaimer

I'm bad at this. Quite bad, actually. I've not developed many operating systems
and I'm not exactly great at low-level programming (as I am an enterprise and
cloud developer most of the time). That being said: I'm trying to learn, and I
hope to become an active developer in this particular field relatively soon.

## What's in here?

Here, you can find my dabblings in operating systems analysis and development
(mostly the former for now). I expect to be working on quite a few projects in
the near future directly related to the development of operating systems and
other related fields.

## What should I expect to see here?

Overviews of a variety of operating systems and some other related topics. You
should see quite a bit of overlap between this section and
[containerisation](/containerisation/README.md).
