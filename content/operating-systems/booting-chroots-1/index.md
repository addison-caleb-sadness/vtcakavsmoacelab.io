+++
title = "Booting chroots: A Brief Introduction"
date = 2019-02-11
+++

I highly suggest you be a little acquainted with chrooting and possibly a little
bit of GRUB2 before continuing.

If you plan to do this, do so at your own risk and understand that this _is_
complicated. Know what you're doing before you accidentally wipe your disk.

## Context

I like Linux black magic. One of my favorite pastimes, actually. For example:

<figure>
<img alt="Black Magic" src="black_magic1.png">
<figcaption>
<p>* Black Magic *</p>
</figcaption>
</figure>

You're looking at an installation of blackarch (on top) installed as a chroot of
an Ubuntu host OS (on bottom). If you want to do this yourself, use
[arch-bootstrap](https://github.com/tokland/arch-bootstrap) and
[over-arch blackarch](https://blackarch.org/downloads.html#install-repo). Takes
about 30 minutes to do fully, depending on your internet speed. I'd suggest
going ahead an dumping a `pacman -Syy blackarch` if you have the 50 GB to spare.

Oh -- and when you chroot in, make sure to do the following mounts:

```bash
# mountblackarch
#!/bin/bash

cd $HOME/blackarch/chroot

sudo mount -t proc proc proc/  # for processes
sudo mount -t sysfs sys sys/   # for the system
sudo mount -o bind /dev dev/   # for devices
sudo mount -o bind /dev/pts dev/pts # for pseudo-terminal devices

sudo touch root/.Xauthority
sudo chroot . su - root -c "xauth add $(xauth list)" # for X forwarding; this'll complain the first time you do it
```

Make sure to clean up the mounts and `.Xauthority` after you're done, too.

## The Problem

A lot of blackarch's strengths come in `systemctl` services -- a lot of them.
And that makes life difficult if you're working in a chroot as it will most
likely come back with

```
Running in chroot, ignoring request
```

Not good.

## The Solution

`systemctl` only really works well when the `systemd` of the OS you're working
in (chroot or no) has a PID of 1 (read: the first process). So we have to boot
into the chroot.

## Some Details

I work in a [LUKS-encrypted](https://guardianproject.info/code/luks/)
[LVM volume group](https://wiki.ubuntu.com/Lvm) that contains both my host
Ubuntu OS (and in turn my blackarch chroot) and a swap volume. That means that I
have to use an [initramfs](https://wiki.debian.org/initramfs) (simply: a small
filesystem (fs) loaded into memory (ram) that contains everything the kernel
needs to get started (init)) which points at my host OS, then somehow transfer
to my chroot as the root directory.

Not only that, but blackarch uses a version of the Linux kernel that's a little
newer than Ubuntu's (read: incompatible modules), so I have to generate the
initramfs in the chroot, transfer it out, then make a custom boot entry that
then invokes the black magic to change roots but keep the PID.

That's a lot of stuff to do. [Let's get cracking!](../booting-chroots-2/)
