+++
title = "TAMU"
+++

Texas A&M University is where I go for my education. I'm working towards a
double major in Computer Science and Mathematics with a minor in Cybersecurity.

## What's in here?

Any of my hijinx associated directly with TAMU will go here. Some things will be
more programming related than TAMU related, so not _everything_ will go here.
