+++
title = "Introduction"
transparent = true
+++

### Welcome to my home!

If you're here for my contact info, you can [email](mailto:me@addisoncrump.info)
me here. If you want to reach out to me another way, I'm most reachable on
[Discord](https://discord.gg/Wr3FhvM),
[LinkedIn](https://www.linkedin.com/in/addison-crump-7262a6149/),
[GitHub](https://github.com/VTCAKAVSMoACE), and
[GitLab](https://gitlab.com/VTCAKAVSMoACE).

### What is this place?

This website is where I will put information about various topics that I'm
currently exploring. This could be anything, really -- just whatever I feel like
putting up here. You can see the categories of topics on the left.

### Who are you, anyways?

My name is Addison Crump. I'm a student at Texas A&M University working towards
a major in Computer Science with a minor in Cybersecurity.

Outside of academia, I have several years of experience as a programmer, a
systems administrator, a security analyst, and a pentester. More information is
available about each of these activities in their respective sections.
