+++
title = "chroots"
transparent = true
date = 2019-02-11
+++

Or: How I Learned to Stop Worrying and Love the Kernel

## A Little Background

chrooting is a beautiful thing. The concept effectively works like this:

1. Make a folder
2. Throw software into it
3. Invoke an executable inside of the folder while fooling it into believing
   that the root of the filesystem is the folder you just made
4. Profit

Of course, it's a little more complicated than that, but functionally, it allows
you to spawn processes in other root filesystems sharing the same kernel.

## What's the point?

Off the top of my head:

- Some software isn't compatible with the host OS
- You want to limit the access to resources that a process has
- You wanna do hoodrat stuff with your friends
- You want to boot into a system from a LiveCD to repair it

And many other things, too, but those are the three things I see most.

## What's the catch?

In low-functionality chroots, where filesystem-emplaced resources aren't needed
(such as /dev, /proc, etc.), a chroot is a perfect solution; it isolates the
program's reach to just that folder. This is a fairly major security benefit.

On the other hand, it still has access to memory, networking (sometimes), other
processes (again, sometimes), and other resources which the kernel shares by
default.

Even bigger problems arise when chrooting into entire OSes; you will have to
mount (most frequently) `/dev`, `/dev/pts`, `/proc`, `/sys`, and occasionally
`/run`. This discloses resources to the chroot that it frequently doesn't need
access to.

Another issue is that quite a few OSes depend on an `init` command being run
with PID 1. For example, services rarely even execute given that they know
they're in a chroot (this is for the user's safety, of course). While PID 1 is
still _a_ `init` command, it's not the one they're expecting and they certainly
don't have direct access to it.

## Workarounds?

Spend some time configuring or use other kinds of "jails", such as
[FreeBSD's jail](https://www.freebsd.org/doc/handbook/jails.html),
[containers](../containerisation/README.md), or even
[systemd-nspawn](https://www.freedesktop.org/software/systemd/man/systemd-nspawn.html),
but be aware that all of these have their own set of caveats as well.

Of course, you could just
[boot straight into the OS](../../operating-systems/booting_chroots/README.md).
