+++
title = "Containerisation"
transparent = true
+++

Containers are extremely cool. Their basic concepts of reliable distribution and
isolation are critical to the next generation of development (at least, that's
what I think).

## What's in here?

You'll see me playing around with containers, exploring mostly docker and its
applications to software development and systems administration. This could
include anything from something as simple as developing with musl to something
as complex as orchestrations in swarms. It honestly just depends on what I delve
into.
