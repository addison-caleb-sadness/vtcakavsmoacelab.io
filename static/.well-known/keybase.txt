==================================================================
https://keybase.io/vtcakavsmoace
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://addisoncrump.info
  * I am vtcakavsmoace (https://keybase.io/vtcakavsmoace) on keybase.
  * I have a public key ASBelGhMpRNE4L-NTlVH7EiiCcLKpG5j1-LnZPGjjCwnTwo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120313da163e78861602d8078b0ff70115cc94799b17d2636e6408039c0e5a66a510a",
      "host": "keybase.io",
      "kid": "01205e94684ca51344e0bf8d4e5547ec48a209c2caa46e63d7e2e764f1a38c2c274f0a",
      "uid": "fa00e9d993749bdbe0845a8f44e09119",
      "username": "vtcakavsmoace"
    },
    "merkle_root": {
      "ctime": 1579034602,
      "hash": "cbef80180b472669d9800cf1ecaf360d331d37629149b6a9c668f2037fa766d151711a0da34759c06687167251a57078ed68c4417474c3bc9c6a6be2e26a9229",
      "hash_meta": "2b039c61554d32c61513fb13cb4c91911751ea5196576ef12c36ed89b88f6790",
      "seqno": 14227249
    },
    "service": {
      "entropy": "S6IdVxLCc999SBPlidpuj1ET",
      "hostname": "addisoncrump.info",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.1.1"
  },
  "ctime": 1579034619,
  "expire_in": 504576000,
  "prev": "a995423619febf3ce434b485d44ae56f4d1abe2cf7ae7bb432d318897b496938",
  "seqno": 42,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgXpRoTKUTROC/jU5VR+xIognCyqRuY9fi52Txo4wsJ08Kp3BheWxvYWTESpcCKsQgqZVCNhn+vzzkNLSF1Erlb00aviz3rnu0MtMYiXtJaTjEIKe+yWPnHrwAzfMLsCuXwacz8oZ8eBFllQcUZDezgYuCAgHCo3NpZ8RASQBxz56Kt/KZU2qLw1/XD/YMprvjvpmxxalJO2BmLsOaaAC+8qWLsU1u6F14Z2EmzvL0HDT1gUB1iLJXwUuyDahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIGY5BvFukRtZNbTs3L9g/6TwYShZ63mWsMkvt3jdsXVzo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/vtcakavsmoace

==================================================================
